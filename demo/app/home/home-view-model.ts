import { Observable } from "tns-core-modules/data/observable";
import { FileDownloader } from 'nativescript-file-downloader';

export class HomeViewModel extends Observable {
    constructor() {
        super();
    }

    downloadFile() {
        const url = 'https://i.guim.co.uk/img/media/d143e03bccd1150ef52b8b6abd7f3e46885ea1b3/0_182_5472_3283/master/5472.jpg?width=1140&quality=85&auto=format&fit=max&s=db5422b6dec1d9db5137ca1d232c7770'

        FileDownloader.download(url).then(
            (fileUrl) => {
                const activityVC = UIActivityViewController.alloc().initWithActivityItemsApplicationActivities([fileUrl], null);
                // Needed for iPad to present, otherwise will crash on iPad
                if (activityVC.popoverPresentationController) {
                    activityVC.popoverPresentationController.sourceView = UIApplication.sharedApplication.keyWindow.rootViewController.view;
                    const rect = {
                        origin: {
                            x: UIApplication.sharedApplication.keyWindow.rootViewController.view.bounds.size.width / 2,
                            y: UIApplication.sharedApplication.keyWindow.rootViewController.view.bounds.size.height / 2,
                        },
                        size: { width: 0, height: 0 },
                    };
                    activityVC.popoverPresentationController.sourceRect = rect;
                    activityVC.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirection.Any
                }
                UIApplication.sharedApplication.keyWindow.rootViewController.presentViewControllerAnimatedCompletion(activityVC, true, null);

            }).catch((error) => {
                console.log(error);
            });
    }
}
