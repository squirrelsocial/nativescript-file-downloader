export declare class FileDownloader {
  static download(downloadUrl: string): Promise<NSURL>
}
