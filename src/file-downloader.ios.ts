import { FileDownloader as FileDownloaderBase } from  "./file-downloader.common";

export class FileDownloader implements FileDownloaderBase {
  static download(downloadUrl: string): Promise<NSURL> {
    return new Promise<NSURL>((resolve, reject) => {
      const url = NSURL.URLWithString(downloadUrl);
      NSURLSession.sharedSession.downloadTaskWithURLCompletionHandler(url, (localUrl, response, error) => {
        if (!localUrl || error) {
          reject(error);
        }

        try {
          let documentDirectory = NSFileManager.defaultManager.URLForDirectoryInDomainAppropriateForURLCreateError(
              NSSearchPathDirectory.DocumentDirectory,
              NSSearchPathDomainMask.UserDomainMask,
              null,
              false
            );

            const fileName = url.lastPathComponent;
            const fileUrl = documentDirectory.URLByAppendingPathComponent(fileName);

            const fileData = NSData.dataWithContentsOfURLOptionsError(localUrl, NSDataReadingOptions.DataReadingMapped)
            // Write the file out temporarily so that the correct file name is preserved
            fileData.writeToURLOptionsError(fileUrl, NSDataWritingOptions.AtomicWrite);

            resolve(fileUrl);

          } catch (error) {
            reject(error);
          }
      }).resume();
    });
  }
}
